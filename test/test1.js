import {
  initLog} from "scripts/utils/log-utils.js";
import { solveContract, codingContractTypesMetadata } from "scripts/daemons/contract.js";

  /** @param {NS} ns **/
export async function main(ns) {
  initLog(ns);
  ns.ls('home', "cct").forEach(filename => ns.rm(filename));
  codingContractTypesMetadata.forEach(contractType => {
      ns.codingcontract.createDummyContract(contractType.name);
   })
   ns.tprint("known contracts number: ", codingContractTypesMetadata.length)
   ns.ls('home', "cct").forEach( fileName => {
    try {
    solveContract(ns, 'home', fileName);
    } catch(err) {
      ns.tprint("ERROR: ", err.message);
    }
  })
}