/** 
 * @param {NS} ns 
 * **/
export function initLog(ns) {
    ns.disableLog("sleep");
    ns.disableLog("scan");
    ns.disableLog("disableLog");
    ns.disableLog("getServerUsedRam");
    ns.disableLog("getServerMaxRam");
    ns.disableLog("scp");
    ns.disableLog("killall");
    ns.disableLog("getServerMoneyAvailable");
    ns.clearLog();
}

/**
 *  @param {NS} ns 
 * */
export function logExitOnce(ns) {
    logExit(ns, "Exiting script execution because 'once' parameter choosen")
}

/**
 *  @param {NS} ns 
 * */
export function logExit(ns, ...msg) {
    logInfo(ns, ...msg);
    ns.exit();
}

/**
 *  @param {NS} ns 
 *  @param {string} msg 
 * */
export function throwError(ns, ...msg) {
    logError(ns, ...msg)
    throw new Error(msg);
}

/**
 *  @param {NS} ns 
 *  @param {string} msg 
 * */
export function logError(ns, ...msg) {
    ns.print("ERROR: ", ...msg);
}

/**
 *  @param {NS} ns 
 *  @param {string} msg 
 * */
export function logSuccess(ns, msg) {
    ns.print("SUCCESS: " + msg);
}

/**
 *  @param {NS} ns 
 *  @param {string} msg 
 * */
export function logWarning(ns, ...msg) {
    ns.print("WARNING: ", ...msg);
}

/**
 *  @param {NS} ns 
 *  @param {string} msg 
 * */
export function logInfo(ns, ...msg) {
    ns.print("INFO: ", ...msg);
}