
export const ANSI_RESET = "\u001B[0m";
export const ANSI_BLACK = "\u001B[30m";
export const ANSI_RED = "\u001B[31m";
export const ANSI_GREEN = "\u001B[32m";
export const ANSI_YELLOW = "\u001B[33m";
export const ANSI_BLUE = "\u001B[34m";
export const ANSI_PURPLE = "\u001B[35m";
export const ANSI_CYAN = "\u001B[36m";
export const ANSI_WHITE = "\u001B[37m";
export const ANSI_YELLOW_BACKGROUND = "\u001B[48;5;3m";
export const ANSI_CYAN_BOLD = "\u001B[36;1m";
export const ANSI_CYAN_UNDERLINE = "\u001B[36;4m";
export const ANSI_BOLD = "\u001B[1m";
//export const ANSI_UNBOLD = "\u001B[21m";
export const ANSI_UNDERLINE = "\u001B[4m";
//export const ANSI_STOP_UNDERLINE = "\u001B[24m";
//export const ANSI_BLINK = "\u001B[5m";
export const ANSI_RED_BOLD_UNDERLINE = "\u001B[38;5;1;1;4m";
export const ANSI_RED_BOLD_UNDERLINE_YELLOW_BACKGROUND = "\u001B[48;5;3;38;5;1;1;4m";

//https://stackoverflow.com/questions/4842424/list-of-ansi-color-escape-sequences


/** @param {NS} ns **/
export async function main(NS) {
NS.tprint(ANSI_BLACK + "ANSI_BLACK")
NS.tprint(ANSI_RED + "ANSI_RED")
NS.tprint(ANSI_GREEN + "ANSI_GREEN")
NS.tprint(ANSI_YELLOW + "ANSI_YELLOW")
NS.tprint(ANSI_YELLOW_BACKGROUND + "ANSI_YELLOW_BACKGROUND")
NS.tprint(ANSI_BLUE + "ANSI_BLUE")
NS.tprint(ANSI_PURPLE + "ANSI_PURPLE")
NS.tprint(ANSI_CYAN + "ANSI_CYAN")
NS.tprint(ANSI_CYAN_BOLD + "ANSI_CYAN_BOLD")
NS.tprint(ANSI_CYAN_UNDERLINE + "ANSI_CYAN_UNDERLINE")
NS.tprint(ANSI_WHITE + "ANSI_WHITE")
NS.tprint(ANSI_RED_BOLD_UNDERLINE + "ANSI_RED_BOLD_UNDERLINE")
NS.tprint(ANSI_RED_BOLD_UNDERLINE_YELLOW_BACKGROUND + "ANSI_RED_BOLD_UNDERLINE_YELLOW_BACKGROUND")
}
