/** 
 * @param {NS} ns
 * @returns {Server[]}
 * */
export function getServers(ns) {
    let result = ['home'];
    for (const element of result) {
        for (let server of ns.scan(element)) {
            if (!result.includes(server)) {
                result.push(server);
            }
        }
    }
    return result.map(x => ns.getServer(x));
}

/** 
 * @param {NS} ns
 * @returns {Server[]}
 * */
export function getTargets(ns) {
    let servers = getServers(ns);
    return servers.filter(server => server.hostname != 'home')
        .filter(server => (server.requiredHackingSkill <= ns.getPlayer().skills.hacking || server.requiredHackingSkill == 1) && server.hasAdminRights && server.moneyMax > 0).sort((a, b) => a.requiredHackingSkill - b.requiredHackingSkill);
}

/** 
 * @param {NS} ns
 * @param {String} target
 * @returns {String}
 * */
export function getServerPath(ns, target) {
    let output = "home; ";
    getServerPathList(ns, target).forEach(server => output += " connect " + server + ";");
    return output;
}

/** 
 * @param {NS} ns
 * @param {String} target
 * @returns {String}
 * */
export function getServerPathList(ns, target) {
    let paths = {
        "home": ""
    };
    let queue = Object.keys(paths);
    let name;
    let pathToTarget = [];
    while ((name = queue.shift())) {
        let path = paths[name];
        let scanRes = ns.scan(name);
        for (let newSv of scanRes) {
            if (paths[newSv] === undefined) {
                queue.push(newSv);
                paths[newSv] = `${path},${newSv}`;
                if (newSv == target)
                    pathToTarget = paths[newSv].substr(1).split(",");

            }
        }
    }
    return pathToTarget;
}