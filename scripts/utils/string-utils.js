/**
 * join strings to linux style path
 * @param {string[]} args
 */
export function pathJoin(...args) {
    return args.join('/').replace(/\/\/+/g, '/');
}

/**
 * join strings to linux style path 
 * @param {string[]} args
 */
export function filePathJoin(...parts) {
    return pathJoin('', args);
}


