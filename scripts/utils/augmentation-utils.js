export const hacking_stats = [
    'h',
    'h_exp',
    'h_chance',
    'h_speed',
    'h_money',
    'h_grow',
]

export const combat_stats = [
    'str',
    'def',
    'dex',
    'agi',
    'cha',
    'str_exp',
    'def_exp',
    'dex_exp',
    'agi_exp',
    'cha_exp',
]

export const rep_stats = ['company_rep',
    'faction_rep',
]

export const other_stats = [
    'crime_money',
    'crime_success',
    'work_money',

    'hacknet_node_money',
    'hacknet_node_purchase_cost',
    'hacknet_node_ram_cost',
    'hacknet_node_core_cost',
    'hacknet_node_level_cost',

    'bladeburner_max_stamina',
    'bladeburner_stamina_gain',
    'bladeburner_analysis',
    'bladeburner_success_chance',
]

/** @param {NS} ns */
export function getNoOwnedAugmentations(ns, factions) {
    return Object.fromEntries(Object.entries(getAllAugmentations(ns, factions)).filter(entry => !entry[1].owned));
}

/** @param {NS} ns */
export function getNoPurchasedAugmentations(ns, factions) {
    return Object.fromEntries(Object.entries(getNoOwnedAugmentations(ns, factions)).filter(entry => !entry[1].purchased));
}

/** @param {NS} ns */
export function getOwnedAugmentations(ns, factions) {
    return Object.fromEntries(Object.entries(getAllAugmentations(ns, factions)).filter(entry => entry[1].owned));
}

/** @param {NS} ns */
export function getAllAugmentations(ns, factions) {
    let augs = {};
    factions
        .forEach(f => {
            ns.singularity.getAugmentationsFromFaction(f.name)
                .forEach(a => augs = putAugmentationData(ns, augs, a, f))
        })
    return augs;
}

/** @param {NS} ns */
export function putAugmentationData(ns, augs, aug, faction) {
    if (typeof augs[aug] == 'undefined') {
        augs[aug] = {
            name: aug,
            stats: Object.entries(ns.singularity.getAugmentationStats(aug)).filter(entry => entry[1] != '1')
                .map(entry => [entry[0].replace('strength', 'str').replace('hacking', 'h').replace('dexterity', 'dex').replace('agility', 'agi').replace('defense', 'def').replace('charisma', 'cha').replace('hacknet_node', 'hacknet'), entry[1]]),
            rep: ns.singularity.getAugmentationRepReq(aug),
            price: ns.singularity.getAugmentationBasePrice(aug),
            prerequisites: ns.singularity.getAugmentationPrereq(aug),
            owned: ns.singularity.getOwnedAugmentations().includes(aug),
            purchased: ns.singularity.getOwnedAugmentations(true).includes(aug),
            factions: []
        }
    }
    augs[aug].factions.push(faction.name)
    return augs
}

/** @param {NS} ns */
export function canPurchaseAug(ns, aug) {
    return aug.factions.filter(f => ns.singularity.getFactionRep(f) >= aug.rep).length > 0
}
