import {
    throwError, logInfo
} from "scripts/utils/log-utils.js";

/**
 *  @param {NS} ns 
 *  @param {Server} host 
 *  @returns {number}
 * */
export async function execScriptAndWait(ns, script, host, numThreads, ...args) {
    let PID1 = execScript(ns, script, host, numThreads, ...args);
    ns.print(PID1);
    if (PID1 == 0) {
        throwError(ns, "script not started: " + script);
    }
    await waitForPID(ns, PID1);
    return PID1;
}

/**
 *  @param {NS} ns 
 *  @param {Server} host 
 *  @returns {number}
 * */
export function execScript(ns, script, host, numThreads, ...args) {
    logInfo(ns, "running ", script, " host:", host.hostname, " threads:", numThreads, " args:" + args);
    let PID = ns.exec(script, host.hostname, numThreads, ...args);
    ns.print(PID);
    return PID;
}

/**
 *  @param {NS} ns 
 *  @param {number} PID 
 * */
export async function waitForPID(ns, PID) {
    while (ns.isRunning(PID)) {
        await ns.sleep(5);
    }
}