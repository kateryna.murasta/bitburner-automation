import {
    getServerPath
} from "scripts/utils/servers-utils";

/**
 *  @param {NS} ns 
 * */
export async function main(ns) {
    ns.tprintf(getServerPath(ns, ns.args[0]));
}

export function autocomplete(data, args) {
    return [...data.servers];
}