import { getFactionInfo, getPossibleFactions } from "scripts/monitors/factions_info";
import { initLog, logExit, logExitOnce } from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false],   // if set script will exit after first try
    ['tail', false]    // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitJoinFactions(ns, flags.timeout, flags.once);
}

/** @param {NS} ns **/
export async function awaitJoinFactions(ns, timeout, once) {
    while (true) {
        if (getPossibleFactions(ns).length == 0) {
            logExit("All factions joined");
        }
        let factions = ns.singularity.checkFactionInvitations();
        factions
        //.filter(faction => cityFaction.ban.filter(banned => banned == faction).length == 0)
        .filter(faction => (ns.singularity.getAugmentationsFromFaction(faction).filter(aug => !ns.singularity.getOwnedAugmentations(true).includes(aug)).length > 0) || (typeof getFactionInfo(faction).ban == "undefined"))
        .forEach(faction => ns.singularity.joinFaction(faction))
        if(once) {
            logExitOnce(ns);
        }
        await ns.sleep(timeout);
    }
}