import { initLog, logExitOnce, logExit } from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false]    // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitTor(ns, flags.timeout, flags.once);
}

/** @param {NS} ns **/
export async function awaitTor(ns, timeout, once) {
    while (true) {
        if(ns.hasTorRouter()) {
            logExit(ns, "TOR already purchased");
        }
        if(ns.singularity.purchaseTor()) {
            logExit(ns, "TOR purchased");
        }
        if(once) {
            logExitOnce(ns);
        }
        await ns.sleep(timeout);
    }
}