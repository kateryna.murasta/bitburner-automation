import { canPurchaseAug, getNoPurchasedAugmentations } from "scripts/utils/augmentation-utils";
import {
    criminalFactions, getAllFactions} from "scripts/monitors/factions_info";

const flagsSchema = [
    ['timeout', 100000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    //initLog(ns);
    await awaitWorkForFactions(ns, flags.timeout);
}

/** @param {NS} ns **/
export async function awaitWorkForFactions(ns, timeout) {
    let faction = '';
    let factions = getAllFactions(ns)
    //let factions = Object.values(criminalFactions)
    .filter(f => ns.getPlayer().factions.includes(f.name));
    do {
        faction = getFactionsForWork(ns, factions);
        if (!ns.singularity.workForFaction(faction, "hacking", false)) {
            if (!ns.singularity.workForFaction(faction, "field", false)) {
                if (!ns.singularity.workForFaction(faction, "security", false)) {
                    delete factions[factions.indexOf(factions.find(f => f.name == faction))]
                    await ns.sleep(100);
                    continue;
                }
            }
        }
        await ns.sleep(timeout);
    }while (faction!='')
}

function getFactionsForWork(ns, factions) {
    if(factions.length == 0) return ''
    let augs = getNoPurchasedAugmentations(ns, Object.values(factions))
    if(Object.keys(augs).length == 0) return ''
    
    let goal = Object.keys(augs).filter(aug => !canPurchaseAug(ns, augs[aug]))
    if(goal.length == 0) return ''
    return augs[goal.reduce((a, b) => augs[a].rep > augs[b].rep ? a : b )]
        .factions
            .reduce((a, b) => { return ns.singularity.getFactionRep(b) > ns.singularity.getFactionRep(a) ? b : a })
}