import { initLog, logSuccess, logInfo } from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false]    // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

const programs = { 
    "BruteSSH.exe": 50, 
    "FTPCrack.exe": 100, 
    "relaySMTP.exe": 250, 
    "HTTPWorm.exe": 500, 
    "SQLInject.exe": 750 };

const morePrograms = { 
    "Formulas.exe": 1000, 
    "DeepscanV1.exe": 75, 
    "DeepscanV2.exe": 400, 
    "ServerProfiler.exe": 75, 
    "AutoLink.exe": 25 };

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitPrograms(ns, flags.timeout, flags.once, programs);
    // if(!flags.once) {
    //     await awaitPrograms(ns, flags.timeout, flags.once, morePrograms);
    // }
}

/** 
 * @param {NS} ns 
 * @param {number} timeout
 * @param {boolean} once
 * **/
export async function awaitPrograms(ns, timeout, once, files) {
    while (true) {
        let buyList = Object.keys(files).filter((program) => !ns.fileExists(program));
        if (buyList.length == 0) {
            logInfo(ns, "Programs already purchased: ", files);
            return;
        }
        
        for (let program of buyList) {
            if (ns.singularity.purchaseProgram(program)) {
                logSuccess(ns, `purchased program ${program}`);
            }
        }
        if (once) { return };
        await ns.sleep(timeout);
    }
}