import {
    initLog,
    logExitOnce
} from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

/** @param {NS} ns **/
export async function main(ns) {
    var flags = ns.flags(flagsSchema);
    initLog(ns);
    await mainLoop(ns, flags.timeout, flags.once);
}

/** 
 * @param {NS} ns 
 * @param {number} timeout
 * @param {boolean} once
 * **/
export async function mainLoop(ns, timeout, once) {
    while (true) {
        ns.hacknet.purchaseNode();
        for (var i = 0; i < ns.hacknet.numNodes(); i++) {
            ns.hacknet.upgradeLevel(i, 1);
            ns.hacknet.upgradeRam(i, 1);
            ns.hacknet.upgradeCore(i, 1);
        }
        if (once) {
            logExitOnce(ns);
            return;
        }
        await ns.sleep(timeout);
    }
}