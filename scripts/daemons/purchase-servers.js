import {
    getServers
} from "scripts/utils/servers-utils";
import {
    initLog
} from "scripts/utils/log-utils.js";
import { logExit, logInfo } from "scripts/utils/log-utils";
let maxRam;

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['tail', false], // if set script will show log window
    ['minRam', 8], // time in miliseconds to wait for next try
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    maxRam = ns.getPurchasedServerMaxRam();
    while (ns.getPurchasedServers().length < ns.getPurchasedServerLimit()) {
        await purchaseServer(ns, flags.minRam, flags.timeout);
    }
    while (getSmallestPurchasedServer(ns).maxRam < maxRam) {
        await upgradeServer(ns, flags.timeout);
    }
}

/** 
 * @param {NS} ns 
 * @param {number} minRam
 *  @param {number} timeout
 * **/
export async function purchaseServer(ns, minRam, timeout) {
    await waitForMoneyPurchase(ns, minRam, timeout);
    ns.purchaseServer("pserv-" + (ns.getPurchasedServers().length), minRam);
}

/** 
 * @param {NS} ns 
 *  @param {number} timeout
 * **/
export async function upgradeServer(ns, timeout) {
    let server = getSmallestPurchasedServer(ns);
    if(server.maxRam * 2 > ns.getPurchasedServerMaxRam()) {
        logExit(ns, "all servers upgraded");
    }
    let moneyNeed = ns.getPurchasedServerUpgradeCost(server.hostname, server.maxRam * 2);
    await waitForMoneyUpgrade(ns, server.hostname, server.maxRam * 2, timeout);
    if(ns.upgradePurchasedServer(server.hostname, server.maxRam * 2)) {
        logInfo(ns, "upgraded server " + server.hostname + " for $" + ns.nFormat(moneyNeed, '0.000a') + " current RAM: " + ns.nFormat(ns.getServerMaxRam(server.hostname) * 1000000000, '0.00b'))
    }
}

/**
 *  @param {NS} ns 
 *  @param {number} ram
 *  @param {number} timeout
 * **/
export async function waitForMoneyPurchase(ns, ram, timeout) {
    while (ns.getPurchasedServerCost(ram) > ns.getPlayer().money) {
        await ns.sleep(timeout);
    }
}

/**
 *  @param {NS} ns 
 *  @param {number} ram
 *  @param {number} timeout
 * **/
export async function waitForMoneyUpgrade(ns, serverName, ram, timeout) {
    while (ns.getPurchasedServerUpgradeCost(serverName, ram) > ns.getServerMoneyAvailable("home")) {
        await ns.sleep(timeout);
    }
}

/** 
 * @param {NS} ns 
 * @returns {Server}
 * **/
export function getSmallestPurchasedServer(ns) {
    return getServers(ns).filter(server => ns.getPurchasedServers().indexOf(server.hostname) >= 0).reduce((prev, cur) => (cur.maxRam < prev.maxRam ? cur : prev));
}