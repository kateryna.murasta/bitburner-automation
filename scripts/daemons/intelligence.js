import { initLog } from "scripts/utils/log-utils";
import { execScriptAndWait } from "scripts/utils/script-utils";


/** @param {NS} ns **/
export async function main(ns) {
    initLog(ns);
    await execScriptAndWait(ns, "/scripts/daemons/join-factions.js", ns.getServer("home"), 1, "--once", "--tail");
    ns.singularity.softReset('/scripts/daemons/intelligence.js');
}