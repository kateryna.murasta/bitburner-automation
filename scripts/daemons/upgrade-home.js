import { initLog, logExitOnce } from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false]    // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitUpgrade(ns, flags.timeout, flags.once);
}

/** @param {NS} ns **/
export async function awaitUpgrade(ns, timeout, once) {
    while (true) {
        ns.singularity.upgradeHomeRam()
        ns.singularity.upgradeHomeCores();
        if(once) {
            logExitOnce(ns);
        }
        await ns.sleep(timeout);
    }
}