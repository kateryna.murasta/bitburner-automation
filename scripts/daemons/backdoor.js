import {
    getServers
} from "scripts/utils/servers-utils";
import {
    connect
} from "scripts/commands/connect";
import {
    logExit,
    logExitOnce,
    logInfo
} from "scripts/utils/log-utils";
import { execScript } from "scripts/utils/script-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}

const backdoorTargetScript = "/scripts/commands/backdoor-target.js"

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    ns.disableLog("ALL");
    ns.clearLog();
    await awaitBackdoor(ns, flags.timeout, flags.once);
}

/** 
 * @param {NS} ns 
 * @param {number} timeout
 * @param {boolean} once
 * **/
export async function awaitBackdoor(ns, timeout, once) {
    while (true) {
        await backdoorAll(ns)
        if (once) {
            logExitOnce(ns)
        };
        await ns.sleep(timeout);
    }
}

/** 
 * @param {NS} ns 
 * **/
export async function backdoorAll(ns) {
    let servers = getServers(ns).filter(x => !x.backdoorInstalled && !x.purchasedByPlayer);
    if (servers.length == 0) {
        logExit(ns, "All servers already have backdoor installed");
    }
    logBackdoorTarget(ns, servers, "BACKDOOR");
    for (let server of servers.filter(server => !ns.isRunning(backdoorTargetScript, "home", server.hostname) && server.hasAdminRights && server.requiredHackingSkill <= ns.getPlayer().skills.hacking)) {
        await backdoorServer(ns, server);
    }
}
/** 
 * @param {NS} ns 
 * **/
export async function backdoorServer(ns, server) {
    logInfo(ns, `Installing backdoor on server ${server.hostname}`);
    connect(ns, server.hostname);
    await ns.sleep(100);
    try{
    execScript(ns, backdoorTargetScript, ns.getServer(), 1, server.hostname);
    await ns.sleep(100);
    }
    catch {}
    ns.singularity.connect("home");
}

/** 
 * @param {NS} ns 
 * @param {Server[]} servers
 * @param {string} title
 * **/
export function logBackdoorTarget(ns, servers, title) {
    ns.clearLog();
    ns.print(`╔${title}(${ns.nFormat(servers.length, '0')})`.padEnd(19, "═") + "╦════╦════╦═════╦═════╦════════╗")
    ns.print("║name              ║lvl ║port║root ║backd║path    ║")
    ns.print("╠══════════════════╬════╬════╬═════╬═════╬════════╣")
    for (let server of servers) {
        ns.print("║" + server.hostname.padEnd(18) +
            "║" + ns.nFormat(server.requiredHackingSkill, '0').padStart(4) +
            "║" + server.openPortCount + "/" + server.numOpenPortsRequired + " " +
            ("║" + server.hasAdminRights).padEnd(6) +
            ("║" + server.backdoorInstalled).padEnd(6) +
            "║" + "link    " + "║");
    }
    ns.print("╚══════════════════╩════╩════╩═════╩═════╩════════╝")
}