import {
    getTargets,
    getServers
} from "scripts/utils/servers-utils.js";
import {
    initLog, logExit
} from "scripts/utils/log-utils.js";


const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['threads', 2000], // if set script will show log window
    ['tail', false], // if set script will show log window
    ['kill', false], // if set script will kill all process on hosts before run template
    ['once', false] // if set script will exit after first try
];

const script = '/scripts/early-hack-template.js';

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    let servers = getServers(ns);
    for (let server of servers) {
        if(flags.kill && server.ramUsed > 0) {
            ns.killall(server.hostname, true);
        }
    }
    await ns.sleep(10);
    while(true) {
        await deployScript(ns, flags.threads);
        if (flags.once) {
            logExit(ns);
            return;
        }
        await ns.sleep(flags.timout)
    }

}

/** 
 * @param {NS} ns 
 * @param {number} maxThreads
 * **/
export async function deployScript(ns, maxThreads) {
    let targets = getTargets(ns);
    targets.forEach(target => {
        let threads = maxThreads;
        while (threads > 0) {
            let filtered = getServers(ns).filter(server => server.hasAdminRights).filter(server => Math.floor((server.maxRam - ns.getServerUsedRam(server.hostname) - reservedRam(ns, server.hostname)) / ns.getScriptRam(script)) > 1 && !ns.isRunning(script, server.hostname, target.hostname));
            if (filtered.length == 0) { break;}
            let server = filtered[0];
            ns.scp(script, server.hostname);
            let serverThreads = Math.min(threads, Math.floor((server.maxRam - ns.getServerUsedRam(server.hostname) - reservedRam(ns, server.hostname)) / ns.getScriptRam(script)));
            ns.exec(script, server.hostname, serverThreads, target.hostname);
            threads -= serverThreads;
        }
    });
}

/** 
 * @param {NS} ns 
 * @param {String} hostname
 * **/
export function reservedRam(ns, hostname) {
    const scripts = [
        '/scripts/daemons/contract.js',
        '/scripts/daemons/hacknet.js',
        '/scripts/daemons/nuke.js',
        '/scripts/daemons/programs.js',
        '/scripts/daemons/purchase-servers.js',
    ];
    if(hostname=="home") {
        let ram = 300;
        scripts.filter(script => !ns.scriptRunning(script, 'home')).forEach(script => ram += ns.getScriptRam(script));
        return ram;
    }
    return 0;
}