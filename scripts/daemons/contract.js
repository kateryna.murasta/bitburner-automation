import {
    initLog
} from "scripts/utils/log-utils.js";
import {
    codingContractTypesMetadata
} from "scripts/commands/solve-contract.js";
import {
    getContracts
} from "scripts/commands/find-contracts";


const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['tail', false], // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    while (true) {
        let contracts = getContracts(ns);
        solveContracts(ns, contracts);
        await ns.sleep(flags.timout)
    }
}

/** @param {NS} ns **/
export function solveContracts(ns, contracts) {
    contracts.forEach(contract => {
        contract.contracts.forEach(filename => solveContract(ns, contract.server.hostname, filename));
    });
}

/** @param {NS} ns **/
export function solveContract(ns, hostName, fileName) {
    const contractType = ns.codingcontract.getContractType(fileName, hostName);
    ns.tprint("contract: ", hostName, " - ", fileName, ": ", contractType)
    ns.tprint("data: ", ns.codingcontract.getData(fileName, hostName));
    let solver = codingContractTypesMetadata.find(metadata => metadata.name == contractType).solver;
    if (solver == null) {
        ns.tprint("ERROR found contract of unkonwn type: ", hostName, ": ", fileName, " - ", contractType);
    }
    let answer = solver(ns, ns.codingcontract.getData(fileName, hostName))
    ns.tprint("answer: ", answer)
    if (ns.codingcontract.attempt(answer, fileName, hostName) == "") {
        ns.tprint("ERROR failed to solve contract: ", hostName, ": ", fileName, " - ", contractType);
    } else {
        ns.tprint("INFO successfully solved contract: ", hostName, ": ", fileName, " - ", contractType);
    }
}