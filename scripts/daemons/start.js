import {
    initLog} from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false] // if set script will show log window
];

const scripts = [
    //'hacknet.js', 
    'purchase-servers.js', 
    'programs.js', 
    'tor.js', 
    'work-for-companies.js', 
    'backdoor.js', 
    'join-factions.js', 
    'upgrade-home.js', 
    'contract.js', 
    "deployer.js --threads 500", 
    "nuke.js"
]


/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitStart(ns, flags.timeout);
}

/** @param {NS} ns **/
export async function awaitStart(ns, timeout) {
    for (let script of scripts.filter(script => !ns.isRunning("/scripts/daemons/" + script.split(' ')[0], 'home', ...script.split(' ').slice(1)))) {
        while (ns.exec("/scripts/daemons/" + script.split(' ')[0], 'home', 1, ...script.split(' ').slice(1)) == 0) {
            await ns.sleep(timeout);
        }
    }
}