import {
    getServers
} from "scripts/utils/servers-utils.js";
import {
    initLog,
    logExit
} from "scripts/utils/log-utils.js";


const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['clean', false], // if set script will clean optimisied servers list
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    ns.read
}
