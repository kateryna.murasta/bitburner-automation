import {
    corpFactions} from "scripts/monitors/factions_info";
import {
    initLog} from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return []
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitWorkForCompanies(ns, flags.timeout);
}

/** @param {NS} ns **/
export async function awaitWorkForCompanies(ns, timeout) {
   for (let faction of Object.values(corpFactions)) {
        while (!ns.getPlayer().factions.includes(faction.name) && !ns.singularity.checkFactionInvitations().includes(faction.name) && ns.singularity.getCompanyRep(faction.requirements.corp.name) < getReputationGoal(ns, faction)) {
            ns.singularity.applyToCompany(faction.requirements.corp.name, "IT");
            if(ns.singularity.getCurrentWork() == null || ns.singularity.getCurrentWork().type!="COMPANY" || ns.singularity.getCurrentWork().companyName != faction.requirements.corp.name) {
                ns.singularity.workForCompany(faction.requirements.corp.name);
            }
            await ns.sleep(timeout);
        }
    }
}

export function getReputationGoal(ns, faction) {
    return ns.getServer(faction.requirements.corp.hostname).backdoorInstalled ? 300000 : 400000;
}