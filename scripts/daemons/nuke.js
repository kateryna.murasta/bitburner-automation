import {
    getServers
} from "scripts/utils/servers-utils.js";
import {
    initLog,
    logExitOnce,
    logExit
} from "scripts/utils/log-utils.js";


const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}

/** @param {NS} ns **/
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns);
    await awaitCrack(ns, flags.timeout, flags.once);
}

/** 
 * @param {NS} ns 
 * @param {number} timeout
 * @param {boolean} once
 * **/
export async function awaitCrack(ns, timeout, once) {
    while (true) {
        crackAll(ns);
        if (once) {
            logExitOnce(ns);
            return;
        }
        await ns.sleep(timeout);
    }
}

/** 
 * @param {NS} ns
 * @returns {boolean} 
 * */
export function crackAll(ns) {
    /** @type {Server[]} */
    let servers = getServers(ns).filter(x => !x.hasAdminRights);
    if (servers.length == 0) {
        logExit(ns, "All servers already have root access");
    }
    servers.forEach(server => crack(ns, server));
}

/** 
 * @param {NS} ns
 * @param {Server} target 
 * */
export function crack(ns, target) {
    const programs = Object.entries({
        "BruteSSH": ns.brutessh,
        "FTPCrack": ns.ftpcrack,
        "relaySMTP": ns.relaysmtp,
        "HTTPWorm": ns.httpworm,
        "SQLInject": ns.sqlinject
    });
    for (let i = 0; i < programs.length; i++) {
        if (target.openPortCount <= i && ns.fileExists(`${programs[i][0]}.exe`)) {
            programs[i][1](target.hostname);
        }
    }
    if (target.numOpenPortsRequired <= target.openPortCount) {
        try {
            ns.nuke(target.hostname)
        } catch {};
    }
}