import {
    execScriptAndWait
} from "/scripts/utils/script-utils";
import {
    initLog,
    throwError
} from "/scripts/utils/log-utils";

const flagsSchema = [
    ['tail', false] // if set script will show log window
];

export function autocomplete(data, args) {
    data.flags(flagsSchema);
    if (args.length == 1)
        return data.servers;
    return [...data.servers] || [];
}

/** @param {NS} ns */
export async function main(ns) {
    await optimize(ns, ns.getServer(ns.args[0]));
}

/** 
 * @param {NS} ns 
 * @param {Server} target 
 * */
export async function optimize(ns, target) {
    initLog(ns);
    const minSecurity = target.minDifficulty;
    const maxMoney = target.moneyMax;
    const weaken_script = '/scripts/commands/weaken.js'
    const grow_script = '/scripts/commands/grow.js'
    if (!ns.scp([weaken_script, grow_script], target.hostname)) {
        throwError(ns, "files are not copied")
    }
    while (target.hackDifficulty > minSecurity) {
        await execScriptAndWait(ns, weaken_script, target, 1, target.hostname);
    }
    while (target.moneyAvailable < maxMoney) {
        await execScriptAndWait(ns, grow_script, target, 1, target.hostname);
        while (target.hackDifficulty > minSecurity) {
            await execScriptAndWait(ns, weaken_script, target, 1, target.hostname);
        }
    }
}