/** @param {NS} ns **/
export async function main(ns) {
    let hostName = ns.args[0];
    let fileName = ns.args[1];
    ns.tprint(solveContract(ns, hostName, fileName));
}

/** @param {NS} ns **/
export function solveContract(ns, hostName, fileName) {
    const contractType = ns.codingcontract.getContractType(fileName, hostName);
    ns.tprint("contract: ", hostName, " - ", fileName, ": ", contractType)
    ns.tprint("data: ", ns.codingcontract.getData(fileName, hostName));
    let solver = codingContractTypesMetadata.find(metadata => metadata.name == contractType).solver;
    let answer = solver(ns, ns.codingcontract.getData(fileName, hostName))
    return answer;
}

export const codingContractTypesMetadata = [{
        name: "Find Largest Prime Factor",
        solver: function (ns, data) {
            let fac = 2;
            let n = data;
            while (n > (fac - 1) * (fac - 1)) {
                while (n % fac === 0) {
                    n = Math.round(n / fac);
                }
                ++fac;
            }

            return (n === 1 ? fac - 1 : n);
        },
    },
    {
        name: "Subarray with Maximum Sum",
        solver: function (ns, data) {
            const nums = data.slice();
            for (let i = 1; i < nums.length; i++) {
                nums[i] = Math.max(nums[i], nums[i] + nums[i - 1]);
            }

            return Math.max(...nums);
        },
    },
    {
        name: "Total Ways to Sum",
        solver: function (ns, data) {
            if (typeof data !== "number") throw new Error("solver expected number");
            const ways = [1];
            ways.length = data + 1;
            ways.fill(0, 1);
            for (let i = 1; i < data; ++i) {
                for (let j = i; j <= data; ++j) {
                    ways[j] += ways[j - i];
                }
            }

            return ways[data];
        },
    },
    {
        name: "Total Ways to Sum II",
        solver: function (ns, data) {
            // https://www.geeksforgeeks.org/coin-change-dp-7/?ref=lbp
            const n = data[0];
            const s = data[1];
            const ways = [1];
            ways.length = n + 1;
            ways.fill(0, 1);
            for (const element of s) {
                for (let j = element; j <= n; j++) {
                    ways[j] += ways[j - element];
                }
            }
            return ways[n];
        },
    },
    {
        name: "Spiralize Matrix",
        solver: function (ns, data) {
            const spiral = [];
            const m = data.length;
            const n = data[0].length;
            let u = 0;
            let d = m - 1;
            let l = 0;
            let r = n - 1;
            let k = 0;
            while (true) {
                // Up
                for (let col = l; col <= r; col++) {
                    spiral[k] = data[u][col];
                    ++k;
                }
                if (++u > d) {
                    break;
                }

                // Right
                for (let row = u; row <= d; row++) {
                    spiral[k] = data[row][r];
                    ++k;
                }
                if (--r < l) {
                    break;
                }

                // Down
                for (let col = r; col >= l; col--) {
                    spiral[k] = data[d][col];
                    ++k;
                }
                if (--d < u) {
                    break;
                }

                // Left
                for (let row = d; row >= u; row--) {
                    spiral[k] = data[row][l];
                    ++k;
                }
                if (++l > r) {
                    break;
                }
            }

            return spiral;
        },
    },
    {
        name: "Array Jumping Game",
        solver: function (ns, data) {
            const n = data.length;
            let i = 0;
            for (let reach = 0; i < n && i <= reach; ++i) {
                reach = Math.max(i + data[i], reach);
            }
            const solution = i === n;
            return solution ? 1 : 0;
        },
    },
    {
        name: "Array Jumping Game II",
        solver: function (ns, data) {
            const n = data.length;
            let reach = 0;
            let jumps = 0;
            let lastJump = -1;
            while (reach < n - 1) {
                let jumpedFrom = -1;
                for (let i = reach; i > lastJump; i--) {
                    if (i + data[i] > reach) {
                        reach = i + data[i];
                        jumpedFrom = i;
                    }
                }
                if (jumpedFrom === -1) {
                    jumps = 0;
                    break;
                }
                lastJump = jumpedFrom;
                jumps++;
            }
            return jumps;
        },
    },
    {
        name: "Merge Overlapping Intervals",
        solver: function (ns, data) {
            const intervals = data.slice();
            intervals.sort((a, b) => {
                return a[0] - b[0];
            });

            const result = [];
            let start = intervals[0][0];
            let end = intervals[0][1];
            for (const interval of intervals) {
                if (interval[0] <= end) {
                    end = Math.max(end, interval[1]);
                } else {
                    result.push([start, end]);
                    start = interval[0];
                    end = interval[1];
                }
            }
            result.push([start, end]);

            return result;
        },
    },
    {
        name: "Generate IP Addresses",
        solver: function (ns, data) {
            if (typeof data !== "string") throw new Error("solver expected string");
            const ret = [];
            for (let a = 1; a <= 3; ++a) {
                for (let b = 1; b <= 3; ++b) {
                    for (let c = 1; c <= 3; ++c) {
                        for (let d = 1; d <= 3; ++d) {
                            if (a + b + c + d === data.length) {
                                const A = parseInt(data.substring(0, a), 10);
                                const B = parseInt(data.substring(a, a + b), 10);
                                const C = parseInt(data.substring(a + b, a + b + c), 10);
                                const D = parseInt(data.substring(a + b + c, a + b + c + d), 10);
                                if (A <= 255 && B <= 255 && C <= 255 && D <= 255) {
                                    const ip = [A.toString(), ".", B.toString(), ".", C.toString(), ".", D.toString()].join("");
                                    if (ip.length === data.length + 3) {
                                        ret.push(ip);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return ret;
        },
    },
    {
        name: "Algorithmic Stock Trader I",
        solver: function (ns, data) {
            let maxCur = 0;
            let maxSoFar = 0;
            for (let i = 1; i < data.length; ++i) {
                maxCur = Math.max(0, (maxCur + data[i] - data[i - 1]));
                maxSoFar = Math.max(maxCur, maxSoFar);
            }

            return maxSoFar.toString();
        },
    },
    {
        name: "Algorithmic Stock Trader II",
        solver: function (ns, data) {
            let profit = 0;
            for (let p = 1; p < data.length; ++p) {
                profit += Math.max(data[p] - data[p - 1], 0);
            }

            return profit.toString();
        },
    },
    {
        name: "Algorithmic Stock Trader III",
        solver: function (ns, data) {
            let hold1 = Number.MIN_SAFE_INTEGER;
            let hold2 = Number.MIN_SAFE_INTEGER;
            let release1 = 0;
            let release2 = 0;
            for (const price of data) {
                release2 = Math.max(release2, hold2 + price);
                hold2 = Math.max(hold2, release1 - price);
                release1 = Math.max(release1, hold1 + price);
                hold1 = Math.max(hold1, price * -1);
            }

            return release2.toString();
        },
    },
    {
        name: "Algorithmic Stock Trader IV",
        solver: function (ns, data) {
            const k = data[0];
            const prices = data[1];

            const len = prices.length;
            if (len < 2) {
                return 0;
            }
            if (k > len / 2) {
                let res = 0;
                for (let i = 1; i < len; ++i) {
                    res += Math.max(prices[i] - prices[i - 1], 0);
                }

                return res;
            }

            const hold = [];
            const rele = [];
            hold.length = k + 1;
            rele.length = k + 1;
            for (let i = 0; i <= k; ++i) {
                hold[i] = Number.MIN_SAFE_INTEGER;
                rele[i] = 0;
            }

            let cur;
            for (let i = 0; i < len; ++i) {
                cur = prices[i];
                for (let j = k; j > 0; --j) {
                    rele[j] = Math.max(rele[j], hold[j] + cur);
                    hold[j] = Math.max(hold[j], rele[j - 1] - cur);
                }
            }

            return rele[k];
        },
    },
    {
        name: "Minimum Path Sum in a Triangle",
        solver: function (ns, data) {
            const n = data.length;
            const dp = data[n - 1].slice();
            for (let i = n - 2; i > -1; --i) {
                for (let j = 0; j < data[i].length; ++j) {
                    dp[j] = Math.min(dp[j], dp[j + 1]) + data[i][j];
                }
            }

            return dp[0];
        },
    },
    {
        name: "Unique Paths in a Grid I",
        solver: function (ns, data) {
            const n = data[0]; // Number of rows
            const m = data[1]; // Number of columns
            const currentRow = [];
            currentRow.length = n;

            for (let i = 0; i < n; i++) {
                currentRow[i] = 1;
            }
            for (let row = 1; row < m; row++) {
                for (let i = 1; i < n; i++) {
                    currentRow[i] += currentRow[i - 1];
                }
            }

            return currentRow[n - 1];
        },
    },
    {
        name: "Unique Paths in a Grid II",
        solver: function (ns, data) {
            const obstacleGrid = [];
            obstacleGrid.length = data.length;
            for (let i = 0; i < obstacleGrid.length; ++i) {
                obstacleGrid[i] = data[i].slice();
            }

            for (let i = 0; i < obstacleGrid.length; i++) {
                for (let j = 0; j < obstacleGrid[0].length; j++) {
                    if (obstacleGrid[i][j] == 1) {
                        obstacleGrid[i][j] = 0;
                    } else if (i == 0 && j == 0) {
                        obstacleGrid[0][0] = 1;
                    } else {
                        obstacleGrid[i][j] = (i > 0 ? obstacleGrid[i - 1][j] : 0) + (j > 0 ? obstacleGrid[i][j - 1] : 0);
                    }
                }
            }

            return obstacleGrid[obstacleGrid.length - 1][obstacleGrid[0].length - 1];
        },
    },
    {
        name: "Shortest Path in a Grid",
        solver: function (ns, data) {
            const width = data[0].length;
            const height = data.length;

            let ways = [];
            ways.push([
                [0, 0]
            ]);

            function getPossibleMoves(way) {
                let current = way[way.length - 1]
                let moves = [
                    [current[0] - 1, current[1]],
                    [current[0] + 1, current[1]],
                    [current[0], current[1] - 1],
                    [current[0], current[1] + 1]
                ]
                moves = moves.filter(move => move[0] >= 0 && move[0] < width && move[1] >= 0 && move[1] < height)
                moves = moves.filter(move => data[move[1]][move[0]] == 0);
                moves = moves.filter(move => way.filter(wayMove => wayMove[0] == move[0] && wayMove[1] == move[1]).length == 0);
                return moves;
            }
            let counter = 0;
            while (ways.length > 0 && ways.filter(way => way[way.length - 1][0] == width - 1 && way[way.length - 1][1] == height - 1).length == 0) {
                counter++;
                let _ways = [];
                ways.forEach(way => {
                    let moves = getPossibleMoves(way);
                    if (moves.length > 0) {
                        moves.forEach(move => {
                            if (ways.filter(way => way.filter(step => step[0] == move[0] && step[1] == move[1]).length > 0).length == 0) {
                                //ns.tprint("adding move" + move);
                                let _way = [];
                                way.forEach(move => _way.push(move))
                                _way.push(move);
                                _ways.push(_way)
                            } else {
                                //ns.tprint("filtered repititive move" + move);
                            }

                        });
                    }
                })
                ways = _ways;
            }

            //ways.forEach(row => ns.tprint(row))

            //data.forEach(row => ns.tprint(row))
            //ns.tprint("target:", width - 1, ",", height - 1)

            if (ways.length == 0) {
                return '';
            }

            let way = ways.filter(way => way[way.length - 1][0] == width - 1 && way[way.length - 1][1] == height - 1)[0];
            //ns.tprint(way)
            let path = '';
            for (let i = 1; i < way.length; i++) {
                let x = way[i][0] - way[i - 1][0]
                let y = way[i][1] - way[i - 1][1]
                let move = '';
                if (x == 1) {
                    move = 'R'
                } else if (x == -1) {
                    move = 'L'
                } else if (y == 1) {
                    move = 'D'
                } else {
                    move = 'U'
                }
                path = path.concat(move);
            }

            //ns.tprint(path);
            return path;
        },
    },
    {
        name: "Sanitize Parentheses in Expression",
        solver: function (ns, data) {
            if (typeof data !== "string") throw new Error("solver expected string");
            let left = 0;
            let right = 0;
            const res = [];

            for (const element of data) {
                if (element === "(") {
                    ++left;
                } else if (element === ")") {
                    left > 0 ? --left : ++right;
                }
            }

            function dfs(
                pair,
                index,
                left,
                right,
                s,
                solution,
                res
            ) {
                if (s.length === index) {
                    if (left === 0 && right === 0 && pair === 0) {
                        for (const element of res) {
                            if (element === solution) {
                                return;
                            }
                        }
                        res.push(solution);
                    }
                    return;
                }

                if (s[index] === "(") {
                    if (left > 0) {
                        dfs(pair, index + 1, left - 1, right, s, solution, res);
                    }
                    dfs(pair + 1, index + 1, left, right, s, solution + s[index], res);
                } else if (s[index] === ")") {
                    if (right > 0) dfs(pair, index + 1, left, right - 1, s, solution, res);
                    if (pair > 0) dfs(pair - 1, index + 1, left, right, s, solution + s[index], res);
                } else {
                    dfs(pair, index + 1, left, right, s, solution + s[index], res);
                }
            }

            dfs(0, 0, left, right, data, "", res);
            return res;
        },
    },
    {
        name: "Find All Valid Math Expressions",
        solver: function (ns, data) {
            const num = data[0];
            const target = data[1];

            function helper(
                res,
                path,
                num,
                target,
                pos,
                evaluated,
                multed,
            ) {
                if (pos === num.length) {
                    if (target === evaluated) {
                        res.push(path);
                    }
                    return;
                }

                for (let i = pos; i < num.length; ++i) {
                    if (i != pos && num[pos] == "0") {
                        break;
                    }
                    const cur = parseInt(num.substring(pos, i + 1));

                    if (pos === 0) {
                        helper(res, path + cur, num, target, i + 1, cur, cur);
                    } else {
                        helper(res, path + "+" + cur, num, target, i + 1, evaluated + cur, cur);
                        helper(res, path + "-" + cur, num, target, i + 1, evaluated - cur, -cur);
                        helper(res, path + "*" + cur, num, target, i + 1, evaluated - multed + multed * cur, multed * cur);
                    }
                }
            }

            const result = [];
            helper(result, "", num, target, 0, 0, 0);
            return result;
        },
    },
    {
        name: "HammingCodes: Integer to Encoded Binary",
        solver: function (ns, data) {
            const enc = [0];
            const data_bits = data.toString(2).split("").reverse();

            data_bits.forEach((e, i, a) => {
                a[i] = parseInt(e);
            });

            let k = data_bits.length;

            /* NOTE: writing the data like this flips the endianness, this is what the
             * original implementation by Hedrauta did so I'm keeping it like it was. */
            for (let i = 1; k > 0; i++) {
                if ((i & (i - 1)) != 0) {
                    enc[i] = data_bits[--k];
                } else {
                    enc[i] = 0;
                }
            }

            let parity = 0;

            /* Figure out the subsection parities */
            for (let i = 0; i < enc.length; i++) {
                if (enc[i]) {
                    parity ^= i;
                }
            }

            parity = parity.toString(2).split("").reverse();
            parity.forEach((e, i, a) => {
                a[i] = parseInt(e);
            });

            /* Set the parity bits accordingly */
            for (let i = 0; i < parity.length; i++) {
                enc[2 ** i] = parity[i] ? 1 : 0;
            }

            parity = 0;
            /* Figure out the overall parity for the entire block */
            for (const element of enc) {
                if (element) {
                    parity++;
                }
            }

            /* Finally set the overall parity bit */
            enc[0] = parity % 2 == 0 ? 0 : 1;

            return enc.join("");
        },
    },
    {
        name: "HammingCodes: Encoded Binary to Integer",
        solver: function (ns, data) {
            let err = 0;
            const bits = [];

            /* TODO why not just work with an array of digits from the start? */
            for (const i in data.split("")) {
                const bit = parseInt(data[i]);
                bits[i] = bit;

                if (bit) {
                    err ^= +i;
                }
            }

            /* If err != 0 then it spells out the index of the bit that was flipped */
            if (err) {
                /* Flip to correct */
                bits[err] = bits[err] ? 0 : 1;
            }

            /* Now we have to read the message, bit 0 is unused (it's the overall parity bit
             * which we don't care about). Each bit at an index that is a power of 2 is
             * a parity bit and not part of the actual message. */

            let result = "";

            for (let i = 1; i < bits.length; i++) {
                /* i is not a power of two so it's not a parity bit */
                if ((i & (i - 1)) != 0) {
                    result += bits[i];
                }
            }

            /* TODO to avoid ambiguity about endianness why not let the player return the extracted (and corrected)
             * data bits, rather than guessing at how to convert it to a decimal string? */
            return parseInt(result, 2);
        },
    },
    {
        name: "Proper 2-Coloring of a Graph",
        solver: function (ns, data) {
            // convert from edges to nodes
            const nodes = new Array(data[0]).fill(0).map(() => [])
            for (const e of data[1]) {
                nodes[e[0]].push(e[1])
                nodes[e[1]].push(e[0])
            }
            // solution graph starts out undefined and fills in with 0s and 1s
            const solution = new Array(data[0]).fill(undefined)
            let oddCycleFound = false
            // recursive function for DFS
            const traverse = (index, color) => {
                if (oddCycleFound) {
                    // leave immediately if an invalid cycle was found
                    return
                }
                if (solution[index] === color) {
                    // node was already hit and is correctly colored
                    return
                }
                if (solution[index] === (color ^ 1)) {
                    // node was already hit and is incorrectly colored: graph is uncolorable
                    oddCycleFound = true
                    return
                }
                solution[index] = color
                for (const n of nodes[index]) {
                    traverse(n, color ^ 1)
                }
            }
            // repeat run for as long as undefined nodes are found, in case graph isn't fully connected
            while (!oddCycleFound && solution.some(e => e === undefined)) {
                traverse(solution.indexOf(undefined), 0)
            }
            if (oddCycleFound) return "[]"; // TODO: Bug #3755 in bitburner requires a string literal. Will this be fixed?
            return solution
        },
    },
    {
        name: "Compression I: RLE Compression",
        solver: function (ns, plain) {
            let solution = '';
            let symbol = '';
            let symbolLength = 0;
            for (let i = 0; i < plain.length; i++) {
                if (symbol != plain[i]) {
                    if (symbolLength > 0) {
                        solution = solution.concat(symbolLength).concat(symbol);
                    }
                    symbol = plain[i];
                    symbolLength = 1;
                } else {
                    if (symbolLength == 9) {
                        solution = solution.concat(symbolLength).concat(symbol);
                        symbolLength = 1;
                    } else {
                        symbolLength += 1;
                    }
                }
            }
            return solution.concat(symbolLength).concat(symbol);
        },
    },
    {
        name: "Compression II: LZ Decompression",
        solver: function (ns, compr) {
            let plain = "";

            for (let i = 0; i < compr.length;) {
                const literal_length = compr.charCodeAt(i) - 0x30;

                if (literal_length < 0 || literal_length > 9 || i + 1 + literal_length > compr.length) {
                    return null;
                }

                plain += compr.substring(i + 1, i + 1 + literal_length);
                i += 1 + literal_length;

                if (i >= compr.length) {
                    break;
                }
                const backref_length = compr.charCodeAt(i) - 0x30;

                if (backref_length < 0 || backref_length > 9) {
                    return null;
                } else if (backref_length === 0) {
                    ++i;
                } else {
                    if (i + 1 >= compr.length) {
                        return null;
                    }

                    const backref_offset = compr.charCodeAt(i + 1) - 0x30;
                    if ((backref_length > 0 && (backref_offset < 1 || backref_offset > 9)) || backref_offset > plain.length) {
                        return null;
                    }

                    for (let j = 0; j < backref_length; ++j) {
                        plain += plain[plain.length - backref_offset];
                    }

                    i += 2;
                }
            }

            return plain;
        },
    },
    {
        name: "Compression III: LZ Compression",
        solver: function (ns, plain) {
            {
                // for state[i][j]:
                //      if i is 0, we're adding a literal of length j
                //      else, we're adding a backreference of offset i and length j
                let cur_state = Array.from(Array(10), () => Array(10).fill(null));
                let new_state = Array.from(Array(10), () => Array(10));

                function set(state, i, j, str) {
                    const current = state[i][j];
                    if (current == null || str.length < current.length) {
                        state[i][j] = str;
                    } else if (str.length === current.length && Math.random() < 0.5) {
                        // if two strings are the same length, pick randomly so that
                        // we generate more possible inputs to Compression II
                        state[i][j] = str;
                    }
                }

                // initial state is a literal of length 1
                cur_state[0][1] = "";

                for (let i = 1; i < plain.length; ++i) {
                    for (const row of new_state) {
                        row.fill(null);
                    }
                    const c = plain[i];

                    // handle literals
                    for (let length = 1; length <= 9; ++length) {
                        const string = cur_state[0][length];
                        if (string == null) {
                            continue;
                        }

                        if (length < 9) {
                            // extend current literal
                            set(new_state, 0, length + 1, string);
                        } else {
                            // start new literal
                            set(new_state, 0, 1, string + "9" + plain.substring(i - 9, i) + "0");
                        }

                        for (let offset = 1; offset <= Math.min(9, i); ++offset) {
                            if (plain[i - offset] === c) {
                                // start new backreference
                                set(new_state, offset, 1, string + String(length) + plain.substring(i - length, i));
                            }
                        }
                    }

                    // handle backreferences
                    for (let offset = 1; offset <= 9; ++offset) {
                        for (let length = 1; length <= 9; ++length) {
                            const string = cur_state[offset][length];
                            if (string == null) {
                                continue;
                            }

                            if (plain[i - offset] === c) {
                                if (length < 9) {
                                    // extend current backreference
                                    set(new_state, offset, length + 1, string);
                                } else {
                                    // start new backreference
                                    set(new_state, offset, 1, string + "9" + String(offset) + "0");
                                }
                            }

                            // start new literal
                            set(new_state, 0, 1, string + String(length) + String(offset));

                            // end current backreference and start new backreference
                            for (let new_offset = 1; new_offset <= Math.min(9, i); ++new_offset) {
                                if (plain[i - new_offset] === c) {
                                    set(new_state, new_offset, 1, string + String(length) + String(offset) + "0");
                                }
                            }
                        }
                    }

                    const tmp_state = new_state;
                    new_state = cur_state;
                    cur_state = tmp_state;
                }

                let result = null;

                for (let len = 1; len <= 9; ++len) {
                    let string = cur_state[0][len];
                    if (string == null) {
                        continue;
                    }

                    string += String(len) + plain.substring(plain.length - len, plain.length);
                    if (result == null || string.length < result.length) {
                        result = string;
                    } else if (string.length == result.length && Math.random() < 0.5) {
                        result = string;
                    }
                }

                for (let offset = 1; offset <= 9; ++offset) {
                    for (let len = 1; len <= 9; ++len) {
                        let string = cur_state[offset][len];
                        if (string == null) {
                            continue;
                        }

                        string += String(len) + "" + String(offset);
                        if (result == null || string.length < result.length) {
                            result = string;
                        } else if (string.length == result.length && Math.random() < 0.5) {
                            result = string;
                        }
                    }
                }

                return result ?? "";
            }
        },
    },
    {
        name: "Encryption I: Caesar Cipher",
        solver: function (ns, data) {
            // data = [plaintext, shift value]
            // build char array, shifting via map and join to final results
            const cipher = [...data[0]]
                .map((a) => (a === " " ? a : String.fromCharCode(((a.charCodeAt(0) - 65 - data[1] + 26) % 26) + 65)))
                .join("");
            return cipher;
        },
    },
    {
        name: "Encryption II: Vigenère Cipher",
        solver: function (ns, data) {
            // data = [plaintext, keyword]
            // build char array, shifting via map and corresponding keyword letter and join to final results
            const cipher = [...data[0]]
                .map((a, i) => {
                    return a === " " ?
                        a :
                        String.fromCharCode(((a.charCodeAt(0) - 2 * 65 + data[1].charCodeAt(i % data[1].length)) % 26) + 65);
                })
                .join("");
            return cipher;
        },
    },
];