/** 
 * @param {NS} ns 
 * **/
export async function main(ns) {
    await ns.singularity.installBackdoor();
    ns.tprint(`${(new Date()).toLocaleString()}: Installed backdoor for server ${ns.args[0]}`);
}