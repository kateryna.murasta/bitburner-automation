import {
    getServerPath,
    getServerPathList
} from "scripts/utils/servers-utils";

export function autocomplete(data, args) {
    return [...data.servers];
}

/** @param {NS} ns **/
export async function main(ns) {
    try {
        connect(ns, ns.args[0]);
    } catch {
        const terminalInput = document.getElementById("terminal-input");
        terminalInput.value = getServerPath(ns, ns.args[0]);
        const handler = Object.keys(terminalInput)[1];
        terminalInput[handler].onChange({
            target: terminalInput
        });
        terminalInput[handler].onKeyDown({
            keyCode: 13,
            preventDefault: () => null
        });
    }
}

/** @param {NS} ns **/
export function connect(ns, target) {
    let path = getServerPathList(ns, target)
    for (let step of path) {
        ns.singularity.connect(step);
    }
}