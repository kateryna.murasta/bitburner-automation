/** @param {NS} ns */
export async function main(ns) {
    ns.disableLog("ALL");
    var paths;
    if (ns.args.length > 0) {
        paths = ns.ls(ns.getHostname(), ns.args[0]);
    } else {
        paths = ns.ls(ns.getHostname());
    }
    paths.forEach(path => {ns.tprint(path)});
//    printResult(ns, paths);
}


export function printResult(ns, paths, tab = 0) {
    let result = [];
    let level = {
        result
    };

    paths.forEach(path => {
        path.split('/').reduce((r, name, i, a) => {
            if (!r[name]) {
                r[name] = {
                    result: []
                };
                r.result.push({
                    name,
                    children: r[name].result
                })
            }

            return r[name];
        }, level)
    })
    for (let line of result) {
        var name = line.name;
        if (name == "") {
            name = "..";
        }
        if (line.children.length > 0) {
            var sufix = new Array(tab).join("―") + "﹁ ";
            ns.tprint(sufix + name)
            printResult(ns, line.children, tab + 1)
        } else {
            var sufix = new Array(tab + 4).join(" ");
            ns.tprint(sufix + name)
        }
    }
}