import {
    getServers
} from "scripts/utils/servers-utils.js";

/** @param {NS} ns **/
export async function main(ns) {
    getContracts(ns).forEach(item => item.contracts.forEach(contract => ns.tprint(item.server.hostname, " ", contract)));
}

/** @param {NS} ns **/
export function getContracts(ns) {
    return getServers(ns).map(server => ({
        server: server,
        contracts: ns.ls(server.hostname, "cct")
    })).filter(server => server.contracts.length > 0)
}