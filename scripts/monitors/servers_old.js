import { initLog } from "scripts/utils/log-utils";

const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false],   // if set script will exit after first try
    ['tail', false],   // if set script will show log window
];

const serverType = {
	target: "target",
	backdoor: "backdoor",
	host: "host",
	path: "path",
	scan: "scan",
	quest: "quest"
}

export function autocomplete(data, args) {
	data.flags(flagsSchema);
	for (let arg of args.slice(-2)) {
		if (arg == "--start") {
			return [...data.servers] || []
		}
	}
	return [...Object.values(serverType)] || []
}

/** @param {NS} ns */
export async function main(ns) {
	var flags = ns.flags(flagsSchema);
	initLog(ns)
	ns.disableLog("ALL");
	var action = ns.args[0];
	if (!action) {
		var action = await ns.prompt("select function", { type: "select", choices: Object.values(serverType) })
	}
	while (true) {
		ns.clearLog();
		switch (action) {
			case serverType.target:
				await printTargets(ns, await getTargets(ns));
				break;
			case serverType.backdoor:
				await printBackdoorTargets(ns);
				break;
			case serverType.host:
				await printHosts(ns);
				break;
			case serverType.path:
				await printPath(ns, args.start);
				break;
			case serverType.scan:
				await printClosest(ns, args.start);
				break;
			case serverType.quest:
				await printQuest(ns);
				break;
			case "all":
			default:
				await printAllServers(ns);

		}

		if (flags.once) {
			return;
		}
		await ns.sleep(flags.timeout);
	}
}

/** @param {NS} ns */
export async function printAllServers(ns) {
	let servers = await getServers(ns);
	ns.print("" + servers.size);
	ns.print(Array.from(servers));
}

/** @param {NS} ns */
export async function printClosest(ns, fromHost) {
	ns.print("NEIGHBOURS of " + fromHost + ": " + ns.scan(fromHost).join(", "));
}

/** @param {NS} ns */
export async function printPath(ns, fromHost) {
	ns.print("PATH to " + fromHost + ": " + getPath(ns, fromHost).join(", "));
}

/** @param {NS} ns */
export async function printTargets(ns, servers) {
	ns.print("╔TARGETS" + ("(" + ns.nFormat(servers.length, '0') + ")").padEnd(5, "═") + "══════╦════╦════╦════╦════╦════╦════╗")
	ns.print("║name              ║lvl ║max$║$   ║mSec║sec ║grow║")
	ns.print("╠══════════════════╬════╬════╬════╬════╬════╬════╣")
	for (let server of servers) {
		var target = server.hostname
		ns.print("║" + target.padEnd(18) + "║"
			+ ns.nFormat(ns.getServerRequiredHackingLevel(target), '0').padStart(4) + "║"
			+ ns.nFormat(ns.getServerMaxMoney(target), '0a').padStart(4) + "║"
			+ ns.nFormat(ns.getServerMoneyAvailable(target), '0a').padStart(4) + "║"
			+ ns.nFormat(ns.getServerMinSecurityLevel(target), '0').padStart(4) + "║"
			+ ns.nFormat(ns.getServerSecurityLevel(target), '0').padStart(4) + "║"
			+ ns.nFormat(ns.getServerGrowth(target), '0').padStart(4) + "║");
	}
	ns.print("╚══════════════════╩════╩════╩════╩════╩════╩════╝")
	//ns.write("/data/hack/targets.txt", servers.map(servers => servers.hostname), "w");
}

/** @param {NS} ns */
export async function printHosts(ns) {
	var servers = await getHosts(ns);
	var title = "HOSTS"
		ns.print("╔" + (title + "(" + ns.nFormat(servers.length, '0') + ")").padEnd(18, "═") + "╦════╦════╦═════╦═════╦════════╗")
	ns.print("║name              ║lvl ║port║root ║RAM  ║path    ║")
	ns.print("╠══════════════════╬════╬════╬═════╬═════╬════════╣")
	for (let server of servers) {
		var path = getPath(ns, server.hostname);
		ns.print("║" + server.hostname.padEnd(18)
			+ "║" + ns.nFormat(server.requiredHackingSkill, '0').padStart(4)
			+ "║" + server.openPortCount + "/" + server.numOpenPortsRequired + " "
			+ ("║" + server.hasAdminRights).padEnd(6)
			+ "║" + ns.nFormat(server.maxRam, '0b').padEnd(5)
			+ "║" + "link    " + "║");
	}
	ns.print("╚══════════════════╩════╩════╩═════╩═════╩════════╝")

	//ns.print("HOSTS(" + hosts.length + "):" + hosts.map(host => "[" + host.maxRam + ",\"" + host.hostname + "\"]"))
}

/** @param {NS} ns */
export async function printBackdoorTargets(ns) {
	var servers = await getBackdoorTargets(ns);
	servers.sort((a, b) => a.hostname.localeCompare(b.hostname));
	printNuke(ns, servers, "BACKDOOR GOALS");
	//ns.print("TARGETS(" + targets.length + "):" + JSON.stringify(targets.map(target => target.hostname + ": " + target.requiredHackingSkill + " {" + getPath(ns, target.hostname) + "}"), null, 4))
}

/** @param {NS} ns 
*/
export function printNuke(ns, servers, title) {
	ns.print(`╔${title}(${ns.nFormat(servers.length, '0')})`.padEnd(19, "═") + "╦════╦════╦═════╦═════╦════════╗")
	ns.print("║name              ║lvl ║port║root ║backd║path    ║")
	ns.print("╠══════════════════╬════╬════╬═════╬═════╬════════╣")
	for (let server of servers) {
		var path = getPath(ns, server.hostname);
		ns.print("║" + server.hostname.padEnd(18)
			+ "║" + ns.nFormat(server.requiredHackingSkill, '0').padStart(4)
			+ "║" + server.openPortCount + "/" + server.numOpenPortsRequired + " "
			+ ("║" + server.hasAdminRights).padEnd(6)
			+ ("║" + server.backdoorInstalled).padEnd(6)
			+ "║" + "link    " + "║");
	}
	ns.print("╚══════════════════╩════╩════╩═════╩═════╩════════╝")
}

/** @param {NS} ns */
export async function printQuest(ns) {
	var targets = await getQuestTargets(ns);
	printNuke(ns, targets, "QUEST");
	var targets = await getCorpTargets(ns);
	printNuke(ns, targets, "CORP");
	//ns.print("QUEST(" + targets.length + "):" + JSON.stringify(targets.map(target => target.hostname + ": " + target.requiredHackingSkill + " {" + getPath(ns, target.hostname) + "}"), null, 4))
}

/** @param {NS} ns */
export function getPath(ns, fromHost) {
	var path = new Array();
	path.push(fromHost);
	var backdoorFound = canConnect(ns, fromHost);
	while (!backdoorFound) {
		var servers = ns.scan(path[path.length - 1]);
		for (var server of servers) {
			if (canConnect(ns, server)) {

				path.push(server);
				backdoorFound = true;
				break;
			}
		}
		if (!backdoorFound) {
			path.push(servers[0]);
		}
	}
	path = path.reverse();
	return path;
}

export function canConnect(ns, host) {
	if (ns.scan(host).filter(result => result == ns.getHostname()).length > 0) {
		return true;
	}
	var server = ns.getServer(host);
	return server.backdoorInstalled || server.purchasedByPlayer;
}

/** @param {NS} ns 
 * @returns {Promise<Server[]>}
*/
export async function getTargets(ns) {
	var servers = await getServersObjects(ns);
	return servers.filter(server => !server.purchasedByPlayer && server.moneyMax > 0 && server.requiredHackingSkill <= ns.getHackingLevel());
}

/** @param {NS} ns 
 * @returns {Promise<Server[]>}
*/
export async function getHosts(ns) {
	var servers = await getServersObjects(ns);
	servers.sort((a, b) => b.maxRam - a.maxRam);
	return servers.filter(server => server.maxRam > 0 && server.hasAdminRights);
}

/** @param {NS} ns 
 * @returns {Promise<Server[]>}
*/
export async function getBackdoorTargets(ns) {
	var servers = await getServersObjects(ns);
	return servers.filter(server => !server.backdoorInstalled && !server.purchasedByPlayer);
}

/** @param {NS} ns 
 * @returns {Promise<Server[]>}
*/
export async function getQuestTargets(ns) {
	//var servers = await getServersObjects(ns);
	var f = ["CSEC", "avmnite-02h", "I.I.I.I", "run4theh111z", "w0r1d_d43m0n"];
	return (await getServersObjects(ns)).filter(server => f.includes(server.hostname));
}

/** @param {NS} ns 
 * @returns {Promise<Server[]>}
*/
export async function getCorpTargets(ns) {
	var f = ["megacorp", "ecorp", "kuai-gong", "4sigma", "nwo", "blade", "omnitek", "b-and-a", "clarkinc", "fulcrumassets"];
	return (await getServersObjects(ns)).filter(server => f.includes(server.hostname));
}

/** @param {NS} ns 
 * @returns {Promise<Set>}
*/
export async function getServers(ns) {
	return doScan(ns, "home", new Set());
}

/** @param {NS} ns
 * @returns {Promise<Server[]>}
 */
export async function getServersObjects(ns) {
	let servers = await getServers(ns);
	let serversObjects = new Array();
	for (var server of servers) {
		serversObjects.push(ns.getServer(server));
	}
	serversObjects.sort((a, b) => a.requiredHackingSkill - b.requiredHackingSkill);
	return serversObjects;
}

function doScan(ns, server, result) {
	if (result.has(server)) {
		return result;
	}
	result.add(server);

	var servers = ns.scan(server);
	for (var i = 0; i < servers.length; i++) {
		result = doScan(ns, servers[i], result);
	}
	return result;
}