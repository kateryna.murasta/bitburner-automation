import {
    getAllFactions,
    specialFactions
} from "scripts/monitors/factions_info";
import {
    ANSI_CYAN_UNDERLINE,
    ANSI_GREEN,
    ANSI_PURPLE,
    ANSI_RED,
    ANSI_WHITE,
    ANSI_YELLOW
} from "scripts/utils/ansi_codes";
import {
    canPurchaseAug,
    combat_stats,
    getNoOwnedAugmentations,
    hacking_stats,
    rep_stats
} from "scripts/utils/augmentation-utils";

/** @param {NS} ns */
export async function main(ns) {
    printAugmentationPlan(ns);

}
/** @param {NS} ns */
function printAugmentationPlan(ns) {
    let plan = getAugmentationPlan(ns);
    ns.tprintf("--------AUG PLAN")
    let joined_factions = {}
    Object.entries(plan).forEach(entry => {
        ns.tprintf("------" + entry[0])
        entry[1].forEach(p => {
            let faction = p[0]
            let augs = p[1]
            let main_aug = Object.values(augs).reduce((a, b) => a.rep > b.rep ? a : b)
            joined_factions[faction] = main_aug.rep
            printPlanFaction(ns, faction, main_aug)
            Object.values(augs).forEach(a => {
                printPlanAugShort(ns, a);
            });
        })
    })

    ns.tprintf("--------Faction PLAN")
    Object.keys(joined_factions).forEach(f => ns.tprint(f, " - ", joined_factions[f]));
}


function printPlanFaction(ns, fact, aug) {
    ns.tprintf((aug.factions.length > 1 ? ANSI_PURPLE : ANSI_CYAN_UNDERLINE) + fact + ANSI_WHITE +
        ' ' + ns.nFormat(aug.rep, '0.000a') + "--" +  aug.prerequisites);
}

function printPlanAugShort(ns, aug) {
    ns.tprintf("    " + (aug.owned ? ANSI_GREEN : (canPurchaseAug(ns, aug) ? ANSI_YELLOW : ANSI_RED)) + aug.name + ":" + ANSI_WHITE + " " + ns.nFormat(aug.price, '$0.000a') + ", stats: " + JSON.stringify(aug.stats) + "\u001b[0m");

}
/** @param {NS} ns */
function getNextAug(ns, augs, candidates, joined_factions) {
    let single_f = candidates.filter(a => augs[a].factions.length == 1);
    let aug
    let fact
    if (single_f.length > 0) {
        fact = augs[single_f.reduce((a, b) => augs[a].rep < augs[b].rep ? a : b)].factions[0]
        aug = single_f
            .filter(a => augs[a].factions.includes(fact))
            .reduce((a, b) => augs[a].rep > augs[b].rep ? a : b)
    } else {
        let joined_f = candidates.filter(a => augs[a].factions.filter(f => Object.keys(joined_factions).includes(f)))
        if (joined_f.length > 0) {
            candidates = joined_f
        }
        aug = candidates.reduce((a, b) => augs[a].rep < augs[b].rep ? a : b);
        fact = augs[aug].factions.reduce((a1, b1) => {
            return joined_factions[b1] > joined_factions[a1] ? b1 : a1
        })
    }
    let nexts = augs[aug].prerequisites.filter(a => Object.keys(augs).includes(a))
    if (nexts.length == 0) {
        return [aug, fact]
    } else {
        return getNextAug(ns, augs, nexts, joined_factions)
    }
}


function getAugmentationsByTheWay(ns, augs, faction, aug) {
    return Object.fromEntries(Object.entries(augs)
        .filter(a => a[1].factions.includes(faction) && a[1].rep <= aug.rep))
}

function getAugmentationPlan(ns) {
    let plan = {};
    let joined_factions = {};
    let factions = getAllFactions().filter(f => Object.values(specialFactions).filter(f1 => f1.name == f.name).length == 0);
    let augs = getNoOwnedAugmentations(ns, factions)

    delete augs["NeuroFlux Governor"];

    function buildPlan(ns, callfunc, alias) {
        plan[alias] = []
        while (callfunc(augs).length > 0) {
            let aug_f = getNextAug(ns, augs, callfunc(augs), joined_factions)
            let faction = aug_f[1]
            let aug_q = getAugmentationsByTheWay(ns, augs, faction, augs[aug_f[0]])
            let aug_q1 = Object.fromEntries(Object.entries(aug_q).filter(entry => entry[1].prerequisites.filter(a => (!Object.keys(aug_q).includes(a)) && Object.keys(augs).includes(a)).length == 0 ))
            plan[alias].push([faction, aug_q1])
            Object.keys(aug_q1).forEach(a => delete augs[a])
        }
    }

    buildPlan(ns, first_augs, "P1");
    buildPlan(ns, rep_augs, 'REP');
    buildPlan(ns, hacking_augs, 'HACK');
    buildPlan(ns, combat_augs, 'COMBAT');
    buildPlan(ns, finish_aug, 'FINISH');
    buildPlan(ns, other_augs, 'OTHER');
    return plan;
}

let first_augs = function (augs) {
    return Object.keys(augs)
        .filter(a => a == "Neuroreceptor Management Implant" ||
            a == "CashRoot Starter Kit")
};

let rep_augs = function (augs) {
    return Object.keys(augs)
        .filter(a => augs[a].stats.filter(s => rep_stats.includes(s[0])).length > 0)
        .filter(a => augs[a].factions.length = 1)
}

let hacking_augs = function (augs) {
    return Object.keys(augs)
        .filter(a => augs[a].stats.filter(s => hacking_stats.includes(s[0])).length > 0)
        .filter(a => augs[a].factions.length = 1)
}

let combat_augs = function (augs) {
    return Object.keys(augs)
        .filter(a => augs[a].stats.filter(s => combat_stats.includes(s[0])).length > 0)
        .filter(a => augs[a].factions.length = 1)
}

let finish_aug = function (augs) {
    return Object.keys(augs)
        .filter(a => a == "The Red Pill")
};

let other_augs = function (augs) {
    return Object.keys(augs)
};