import {
    getAllFactions,
    specialFactions
} from "scripts/monitors/factions_info";
import {
    ANSI_CYAN,
    ANSI_GREEN,
    ANSI_RED,
    ANSI_RESET,
    ANSI_WHITE,
    ANSI_YELLOW
} from "scripts/utils/ansi_codes";
import {
    canPurchaseAug,
    getAllAugmentations,
    getNoOwnedAugmentations,
    rep_stats
} from "scripts/utils/augmentation-utils";

/** @param {NS} ns */
export async function main(ns) {
    printNotOwned(ns, getNoOwnedAugmentations(ns, getAllFactions()
        .filter(f => ns.getPlayer().factions.includes(f.name))
    ));

    ns.tprint("---------")

    printAugmentations(ns, getAllAugmentations(ns,
        getAllFactions()
        .filter(f => Object.values(specialFactions).filter(f1 => f1.name == f.name).length == 0)));
}

/** @param {NS} ns */
function printAugmentations(ns, augs) {
    Object.values(augs)
        .sort((a, b) => a.price - b.price)
        .forEach(a => {
            printAugFull(ns, a)
        })
}


/** @param {NS} ns */
function printNotOwned(ns, augs) {
    Object.values(augs)
        .filter(a => a.stats.filter(stat => rep_stats.includes(stat[0])
            //      || combat_stats.includes(stat[0]) 
            //       || hacking_stats.includes(stat[0])
        ).length > 0)
        .sort((a, b) => a.price - b.price)
        .forEach(a => {
            printAugShort(ns, a)
        })
}

function printAugShort(ns, aug) {
    let f = aug.factions.reduce((a, b) => {
        return ns.singularity.getFactionRep(b) > ns.singularity.getFactionRep(a) ? b : a
    })
    ns.tprintf(
        (aug.owned || aug.purchased ? ANSI_GREEN : (canPurchaseAug(ns, aug) ? ANSI_YELLOW : ANSI_RED)) + aug.name +
        (aug.owned ? " (owned)" : (aug.purchased ? " (purchased)" : " (not purchased)")) + ANSI_WHITE + ":" +
        ns.nFormat(aug.price, '$0.000a') + ", " +
        (aug.factions.length > 1 ? ANSI_RESET : ANSI_CYAN) + f + ' ' + ns.nFormat(ns.singularity.getFactionRep(f), '0,0') + '/' + ns.nFormat(aug.rep, '0,0') + ' (' + ns.nFormat(ns.singularity.getFactionFavor(f), '0') + ')')
        ns.tprintf(ANSI_WHITE + "    stats: " + JSON.stringify(aug.stats));
}

function printAugFull(ns, aug) {
    ns.tprintf(
        (aug.owned || aug.purchased ? ANSI_GREEN : (canPurchaseAug(ns, aug) ? ANSI_YELLOW : ANSI_RED)) + aug.name +
        (aug.owned ? " (owned)" : (aug.purchased ? " (purchased)" : " (not purchased)")) + ANSI_WHITE + ":")
    ns.tprintf(ANSI_WHITE + "    rep: " + ns.nFormat(aug.rep, '0.000a') +
    " price: " + ns.nFormat(aug.price, '$0.000a') +
    " prerequisites: " + JSON.stringify(aug.prerequisites))
    ns.tprintf(ANSI_WHITE + "    factions: " + (aug.factions.length > 1 ? ANSI_RESET : ANSI_CYAN) + JSON.stringify(aug.factions))
    ns.tprintf(ANSI_WHITE + "    stats: " + JSON.stringify(aug.stats));
}