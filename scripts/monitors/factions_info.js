import {
    initLog
} from 'scripts/utils/log-utils';

export const specialFactions = {
    Bladeburners: {
        name: 'Bladeburners',
        requirements: {}
    },
    ChurchOfTheMachineGod: {
        name: 'Church of the Machine God',
        requirements: {}
    },
}

const earlyFactions = {
    CyberSec: {
        name: 'CyberSec',
        requirements: {
            backdoor: 'CSEC'
        }
    },
    TianDiHui: {
        name: 'Tian Di Hui',
        requirements: {
            money: 1000000,
            hackLevel: 50,
            cities: ['Chongqing', 'New Tokyo', 'Ishima']
        }
    },
    Netburners: {
        name: 'Netburners',
        requirements: {
            hackLevel: 80,
            hacknet: {
                levels: 100,
                RAM: 8,
                cores: 4
            }
        }
    },
    ShadowsOfAnarchy: {
        name: 'Shadows of Anarchy',
        requirements: {
            infiltration: true
        }
    }
}

export const cityFactions = {
    Aevum: {
        name: 'Aevum',
        requirements: {
            cities: 'Aevum',
            money: 40000000
        },
        ban: ['Chongqing', 'New Tokyo', 'Ishima', 'Volhaven']
    },
    Chongqing: {
        name: 'Chongqing',
        requirements: {
            cities: 'Chongqing',
            money: 20000000
        },
        ban: ['Sector-12', 'Aevum', 'Volhaven']
    },
    Ishima: {
        name: 'Ishima',
        requirements: {
            cities: 'Ishima',
            money: 30000000
        },
        ban: ['Sector-12', 'Aevum', 'Volhaven']
    },
    NewTokyo: {
        name: 'New Tokyo',
        requirements: {
            cities: 'New Tokyo',
            money: 20000000
        },
        ban: ['Sector-12', 'Aevum', 'Volhaven']
    },
    Sector12: {
        name: 'Sector-12',
        requirements: {
            cities: 'Sector-12',
            money: 15000000
        },
        ban: ['Chongqing', 'New Tokyo', 'Ishima', 'Volhaven']
    },
    Volhaven: {
        name: 'Volhaven',
        requirements: {
            cities: 'Volhaven',
            money: 50000000
        },
        ban: ['Chongqing', 'New Tokyo', 'Ishima', 'Sector-12', 'Aevum']
    }
}

const hackFactions = {
    BitRunners: {
        name: 'BitRunners',
        requirements: {
            backdoor: 'run4theh111z',
        }
    },
    TheBlackHand: {
        name: 'The Black Hand',
        requirements: {
            backdoor: 'I.I.I.I',
        }
    },
    NiteSec: {
        name: 'NiteSec',
        requirements: {
            backdoor: 'avmnite-02h',
        }
    }
}

export const corpFactions = {
    BachmanAssociates: {
        name: 'Bachman & Associates',
        requirements: {
            corp: {
                name: 'Bachman & Associates',
                city: 'Aevum',
                hostname: 'b-and-a'
            }
        }
    },
    ECorp: {
        name: 'ECorp',
        requirements: {
            corp: {
                name: 'ECorp',
                city: 'Aevum',
                hostname: 'ecorp'
            }
        }
    },
    FulcrumSecretTechnologies: {
        name: 'Fulcrum Secret Technologies',
        requirements: {
            corp: {
                name: 'Fulcrum Technologies',
                city: 'Aevum',
                hostname: "fulcrumtech"
            },
            backdoor: 'fulcrumassets'
        }
    },
    MegaCorp: {
        name: 'MegaCorp',
        requirements: {
            corp: {
                name: 'MegaCorp',
                city: 'Sector-12',
                hostname: 'megacorp'
            }
        }
    },
    BladeIndustries: {
        name: 'Blade Industries',
        requirements: {
            corp: {
                name: 'Blade Industries',
                city: 'Sector-12',
                hostname: "blade"
            }
        }
    },
    NWO: {
        name: 'NWO',
        requirements: {
            corp: {
                name: 'NWO',
                city: 'Volhaven',
                hostname: 'nwo'
            }
        }
    },
    ClarkeIncorporated: {
        name: 'Clarke Incorporated',
        requirements: {
            corp: {
                name: 'Clarke Incorporated',
                city: 'Aevum',
                hostname: "clarkinc"
            }
        }
    },
    OmniTekIncorporated: {
        name: 'OmniTek Incorporated',
        requirements: {
            corp: {
                name: 'OmniTek Incorporated',
                city: 'Volhaven',
                hostname: "omnitek"
            }
        }
    },
    FourSigma: {
        name: 'Four Sigma',
        requirements: {
            corp: {
                name: 'Four Sigma',
                city: 'Sector-12',
                hostname: "4sigma"
            }
        }
    },
    KuaiGongInternational: {
        name: 'KuaiGong International',
        requirements: {
            corp: {
                name: 'KuaiGong International',
                city: 'Chongqing',
                hostname: "kuai-gong"
            }
        }
    },
}

export const criminalFactions = {
    SpeakersForTheDead: {
        name: 'Speakers for the Dead',
        requirements: {
            karma: -45,
            kills: 30,
            hackLevel: 100,
            stats: 300,
            banedCorps: ['CIA', 'NSA']
        }
    },
    TheDarkArmy: {
        name: 'The Dark Army',
        requirements: {
            karma: -45,
            kills: 5,
            hackLevel: 300,
            stats: 300,
            cities: ['Chongqing'],
            banedCorps: ['CIA', 'NSA']
        }
    },
    TheSyndicate: {
        name: 'The Syndicate',
        requirements: {
            karma: -90,
            money: '10m',
            hackLevel: 200,
            cities: ['Aevum', 'Sector-12'],
            stats: 200,
            banedCorps: ['CIA', 'NSA']
        }
    },
    Silhouette: {
        name: 'Silhouette',
        requirements: {
            karma: -22,
            money: '15m',
            cto: true //CTO, CFO, or CEO of a company
        }
    },
    Tetrads: {
        name: 'Tetrads',
        requirements: {
            karma: -18,
            stats: 75,
            cities: ['Chongqing', 'New Tokyo', 'Ishima'],
        }
    },
    SlumSnakes: {
        name: 'Slum Snakes',
        requirements: {
            stats: 30,
            money: '1m',
            karma: -9,
        }
    },
}

const endGameFaction = {
    Illuminati: {
        name: 'Illuminati',
        requirements: {
            augmentations: 30,
            money: 150000000000,
            hackLevel: 1500,
            stats: 1200,
        }
    },
    Daedalus: {
        name: 'Daedalus',
        requirements: {
            augmentations: 30,
            money: 100000000000,
        },
        altRequirements: {
            hackLevel: 2500,
            stats: 1500,
        }
    },
    TheCovenant: {
        name: 'The Covenant',
        requirements: {
            augmentations: 20,
            money: 75000000000,
            hackLevel: 850,
            stats: 850,
        }
    }
}




/** @param {NS} ns */
export async function main(ns) {
    initLog(ns);
    printFactions(ns, getPossibleFactions(ns));
}

/** @param {NS} ns */
export function printFactions(ns, factions) {
    factions.forEach(faction => {
        let fact_augs = ns.singularity.getAugmentationsFromFaction(faction.name)
        let not_owned_augs = fact_augs.filter(aug => !ns.singularity.getOwnedAugmentations(true).includes(aug))
        ns.tprintf("\u001b[36m" + faction.name + '(' + (fact_augs.length - not_owned_augs.length) + '/' + fact_augs.length + ") :\u001b[0m");

        if (not_owned_augs.length > 0) {
            printRequirement(ns, faction.requirements)
            if (typeof faction.altRequirements != "undefined") {
                printRequirement(ns, faction.altRequirements)
            }
        }

        //ns.print(ns.singularity.getAugmentationsFromFaction(faction.name))
    });
}

/** @param {NS} ns */
export function printRequirement(ns, requirements) {
    if (typeof requirements.hackLevel != "undefined" && ns.getPlayer().skills.hacking < requirements.hackLevel) {
        ns.tprintf("    HackLevel: " + ns.getPlayer().skills.hacking + "/" + requirements.hackLevel)
    }
    if (typeof requirements.stats != 'undefined' && ns.getPlayer().skills.strength < requirements.stats) {
        ns.tprintf("    Strength: " + ns.getPlayer().skills.strength + '/' + requirements.stats);
    }
    if (typeof requirements.stats != 'undefined' && ns.getPlayer().skills.defense < requirements.stats) {
        ns.tprintf("    Defense: " + ns.getPlayer().skills.defense + '/' + requirements.stats);
    }
    if (typeof requirements.stats != 'undefined' && ns.getPlayer().skills.dexterity < requirements.stats) {
        ns.tprintf("    Dexterity: " + ns.getPlayer().skills.dexterity + '/' + requirements.stats);
    }
    if (typeof requirements.stats != 'undefined' && ns.getPlayer().skills.agility < requirements.stats) {
        ns.tprintf("    Agility: " + ns.getPlayer().skills.agility + '/' + requirements.stats);
    }
    if (typeof requirements.backdoor != 'undefined' && !ns.getServer(requirements.backdoor).backdoorInstalled) {
        ns.tprintf("    Backdoor: " + requirements.backdoor + " HackLevel: " + ns.getPlayer().skills.hacking + "/" + ns.getServer(requirements.backdoor).requiredHackingSkill);
    }
    if (typeof requirements.money != 'undefined' && ns.getPlayer().money < requirements.money) {
        ns.tprintf("    Money: " + ns.getPlayer().money + "/" + requirements.money);
    }
    if (typeof requirements.cities != 'undefined') {
        ns.tprintf("    Cities: " + requirements.cities);
    }
    if (typeof requirements.hacknet != 'undefined') {
        ns.tprintf("    Hacknet: ".concat(Object.values(requirements.hacknet).join('_')));
    }
    if (typeof requirements.infiltration != 'undefined') {
        ns.tprintf("    Infiltration: NA");
    }
    if (typeof requirements.karma != 'undefined' && ns.heart.break() > requirements.karma) {
        ns.tprintf("    Karma: " + ns.heart.break() + '/' + requirements.karma);
    }
    if (typeof requirements.kills != 'undefined' && ns.getPlayer().numPeopleKilled < requirements.kills) {
        ns.tprintf("    Kills: " + ns.getPlayer().numPeopleKilled + '/' + requirements.kills);
    }
    if (typeof requirements.banedCorps != 'undefined') {
        ns.tprintf("    banedCorps: " + requirements.banedCorps);
    }
    if (typeof requirements.cto != 'undefined') {
        ns.tprintf("    CTO, CFO, or CEO of a company: NA");
    }
    if (typeof requirements.corp != 'undefined') {
        ns.tprintf("    " + requirements.corp.name + "(" + requirements.corp.city + ") : " + ns.singularity.getCompanyRep(requirements.corp.name) + "/400000.0");
    }
    if (typeof requirements.augmentations != 'undefined' && ns.singularity.getOwnedAugmentations().length < requirements.augmentations) {
        ns.tprintf("    Augmentations: " + ns.singularity.getOwnedAugmentations().length + "/" + requirements.augmentations);
    }
}



export function getPossibleFactions(ns) {
    return getAllFactions()
    .filter(faction => !ns.getPlayer().factions.includes(faction.name))
    .filter(faction => typeof faction.ban == "undefined" || faction.ban.filter(factionName => ns.getPlayer().factions.includes(factionName)).length == 0);
}

export function getAllFactions() {
    let possibleFactions = [];

    possibleFactions.push(...Object.values(earlyFactions));

    possibleFactions.push(...Object.values(cityFactions));

    possibleFactions.push(...Object.values(hackFactions));

    possibleFactions.push(...Object.values(corpFactions));

    possibleFactions.push(...Object.values(criminalFactions));

    possibleFactions.push(...Object.values(endGameFaction));

    possibleFactions.push(...Object.values(specialFactions));
    return possibleFactions;
}

export function getFactionInfo(factionName) {
    return getAllFactions().filter(faction => faction.name==factionName)[0]
}