import { corpFactions } from "scripts/monitors/factions_info";

/** @param {NS} ns */
export async function main(ns) {
    printCompanies(ns, Object.values(corpFactions).map(corp => corp.requirements.corp.name));
    //printCompanies(ns, ns.enums.LocationName);
}
/** 
 * @param {NS} ns 
 * @param {String[]} companies
 * */
function printCompanies(ns, companies) {
    companies.forEach(company => {
        try {
        ns.tprintf(company.padEnd(30, " ") + ": rep - " + ns.nFormat(ns.singularity.getCompanyRep(company), '0,0').padEnd(10, " ") + "\t favor - " + ns.nFormat(ns.singularity.getCompanyFavor(company), '0'))
        } catch {}
    })
}
