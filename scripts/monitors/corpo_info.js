import {
    initLog
} from 'scripts/utils/log-utils';

import {
    corpFactions
} from 'scripts/monitors/factions_info'


const flagsSchema = [
    ['timeout', 1000], // time in miliseconds to wait for next try
    ['once', false], // if set script will exit after first try
    ['tail', false], // if set script will show log window
];


export function autocomplete(data, args) {
    data.flags(flagsSchema);
    return [];
}


/** @param {NS} ns */
export async function main(ns) {
    let flags = ns.flags(flagsSchema);
    initLog(ns, flags.tail)
    ns.disableLog('ALL');
    getCompanies(ns).forEach(company => {
        ns.tprint(company);
    })

}

/** @param {NS} ns */
export function getCompanies(ns) {
    return [
        {
          city: ns.enums.CityName.Aevum,     
          name: ns.enums.LocationName.AevumAeroCorp
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumBachmanAndAssociates,
          faction: corpFactions.BachmanAssociates.name
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumClarkeIncorporated,
          faction: corpFactions.ClarkeIncorporated.name
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumECorp,
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumFulcrumTechnologies,
          faction: corpFactions.FulcrumSecretTechnologies.name
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumGalacticCybersystems,
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumNetLinkTechnologies,
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumPolice,
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumRhoConstruction,
        },
        {
          city: ns.enums.CityName.Aevum,
          name: ns.enums.LocationName.AevumWatchdogSecurity,
        },
        {
          city: ns.enums.CityName.Chongqing,
          name: ns.enums.LocationName.ChongqingKuaiGongInternational,
          faction: corpFactions.KuaiGongInternational.name
        },
        {
          city: ns.enums.CityName.Chongqing,
          name: ns.enums.LocationName.ChongqingSolarisSpaceSystems,
        },
        {
          city: ns.enums.CityName.Ishima,
          name: ns.enums.LocationName.IshimaNovaMedical,
        },
        {
          city: ns.enums.CityName.Ishima,
          name: ns.enums.LocationName.IshimaOmegaSoftware,
        },
        {
          city: ns.enums.CityName.Ishima,
          name: ns.enums.LocationName.IshimaStormTechnologies,
        },
        {
          city: ns.enums.CityName.NewTokyo,
          name: ns.enums.LocationName.NewTokyoDefComm,
        },
        {
          city: ns.enums.CityName.NewTokyo,
          name: ns.enums.LocationName.NewTokyoGlobalPharmaceuticals,
        },
        {
          city: ns.enums.CityName.NewTokyo,
          name: ns.enums.LocationName.NewTokyoNoodleBar,
        },
        {
          city: ns.enums.CityName.NewTokyo,
          name: ns.enums.LocationName.NewTokyoVitaLife,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12AlphaEnterprises,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12BladeIndustries,
          faction: corpFactions.BladeIndustries.name
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12CIA,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12CarmichaelSecurity,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12DeltaOne,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12FoodNStuff,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12FourSigma,
          faction: corpFactions.FourSigma.name
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12IcarusMicrosystems,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12JoesGuns,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12MegaCorp,
          faction: corpFactions.MegaCorp.name
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12NSA,
        },
        {
          city: ns.enums.CityName.Sector12,
          name: ns.enums.LocationName.Sector12UniversalEnergy,
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenCompuTek,
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenHeliosLabs,
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenLexoCorp,
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenNWO,
          faction: corpFactions.NWO.name
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenOmniTekIncorporated,
          faction: corpFactions.OmniTekIncorporated.name
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenOmniaCybersystems,
        },
        {
          city: ns.enums.CityName.Volhaven,
          name: ns.enums.LocationName.VolhavenSysCoreSecurities,
        },
    ];
}